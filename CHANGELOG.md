## [1.4.34](https://github.com/dhis2/user-app/compare/v1.4.33...v1.4.34) (2022-11-20)


### Bug Fixes

* **translations:** sync translations from transifex (master) ([87ab589](https://github.com/dhis2/user-app/commit/87ab589257714a176866ee7d307634d61c3ae8ab))

## [1.4.33](https://github.com/dhis2/user-app/compare/v1.4.32...v1.4.33) (2022-11-19)


### Bug Fixes

* **translations:** sync translations from transifex (master) ([c4af1cf](https://github.com/dhis2/user-app/commit/c4af1cf80ca8fbfb8b84ddf00fa4093f6f51f3a8))

## [1.4.32](https://github.com/dhis2/user-app/compare/v1.4.31...v1.4.32) (2022-11-18)


### Bug Fixes

* address various style and text issues ([#1091](https://github.com/dhis2/user-app/issues/1091)) ([d7295d3](https://github.com/dhis2/user-app/commit/d7295d34071d876ee1d9c50a163cf439641f09ee)), closes [#1094](https://github.com/dhis2/user-app/issues/1094)
* use dedicated endpoint for checking the username ([#1092](https://github.com/dhis2/user-app/issues/1092)) ([e107e02](https://github.com/dhis2/user-app/commit/e107e021dfd7c8342fd1bbe96d95c14620d0e039))

## [1.4.31](https://github.com/dhis2/user-app/compare/v1.4.30...v1.4.31) (2022-11-18)


### Bug Fixes

* **translations:** sync translations from transifex (master) ([399d074](https://github.com/dhis2/user-app/commit/399d0741a4517124d1e7114651e0a26629d9989b))

## [1.4.30](https://github.com/dhis2/user-app/compare/v1.4.29...v1.4.30) (2022-11-17)


### Bug Fixes

* user manager checkbox behaviour ([#1073](https://github.com/dhis2/user-app/issues/1073)) ([dbdf276](https://github.com/dhis2/user-app/commit/dbdf276cc92bc811659b1682bacdc585630b81ee))

## [1.4.29](https://github.com/dhis2/user-app/compare/v1.4.28...v1.4.29) (2022-11-17)


### Bug Fixes

* **translations:** sync translations from transifex (master) ([6606706](https://github.com/dhis2/user-app/commit/660670622fd52106046dbb2ef72c492d2ef2ba53))

## [1.4.28](https://github.com/dhis2/user-app/compare/v1.4.27...v1.4.28) (2022-11-16)


### Bug Fixes

* **translations:** sync translations from transifex (master) ([6a1eddd](https://github.com/dhis2/user-app/commit/6a1eddda90fbd87ab64eab343c76109dd44e0bd6))

## [1.4.27](https://github.com/dhis2/user-app/compare/v1.4.26...v1.4.27) (2022-10-26)


### Bug Fixes

* **translations:** sync translations from transifex (master) ([c5053b9](https://github.com/dhis2/user-app/commit/c5053b9a3db291179a0b1280252f4711fc18bf83))

## [1.4.26](https://github.com/dhis2/user-app/compare/v1.4.25...v1.4.26) (2022-10-22)


### Bug Fixes

* **translations:** sync translations from transifex (master) ([f598517](https://github.com/dhis2/user-app/commit/f598517c5190746a0d3652d53a1925c2da4bb0af))

## [1.4.25](https://github.com/dhis2/user-app/compare/v1.4.24...v1.4.25) (2022-10-07)


### Bug Fixes

* **translations:** sync translations from transifex (master) ([7a5a54d](https://github.com/dhis2/user-app/commit/7a5a54d0c00c17b74de51716d28a75be4878999c))

## [1.4.24](https://github.com/dhis2/user-app/compare/v1.4.23...v1.4.24) (2022-10-05)


### Bug Fixes

* **styled-jsx:** remove incorrect extension ([#1054](https://github.com/dhis2/user-app/issues/1054)) ([a766624](https://github.com/dhis2/user-app/commit/a7666243bf6073798eeae257666e76ffa5801a68))

## [1.4.23](https://github.com/dhis2/user-app/compare/v1.4.22...v1.4.23) (2022-09-28)


### Bug Fixes

* upgrade @dhis2/ui to use correct username validation ([#1051](https://github.com/dhis2/user-app/issues/1051)) ([2469550](https://github.com/dhis2/user-app/commit/2469550de77df918cc8f4efd766b683582100354))

## [1.4.22](https://github.com/dhis2/user-app/compare/v1.4.21...v1.4.22) (2022-08-27)


### Bug Fixes

* **translations:** sync translations from transifex (master) ([82b620c](https://github.com/dhis2/user-app/commit/82b620c06da54cfe9fdc0e3b0e6f6d30d881928d))

## [1.4.21](https://github.com/dhis2/user-app/compare/v1.4.20...v1.4.21) (2022-08-26)


### Bug Fixes

* **translations:** sync translations from transifex (master) ([273afa4](https://github.com/dhis2/user-app/commit/273afa4c41fb9d2a30a6c01c1cc15dcb1de752b3))

## [1.4.20](https://github.com/dhis2/user-app/compare/v1.4.19...v1.4.20) (2022-08-25)


### Bug Fixes

* **translations:** sync translations from transifex (master) ([c351410](https://github.com/dhis2/user-app/commit/c3514103f8694c9f9efa289b0ef077aaa492c765))

## [1.4.19](https://github.com/dhis2/user-app/compare/v1.4.18...v1.4.19) (2022-07-14)


### Bug Fixes

* **bulk-user-manager:** sort list by firstName and surname ([b5f4bf2](https://github.com/dhis2/user-app/commit/b5f4bf27163c1900bae30fc71aedd385a71e2d71))

## [1.4.18](https://github.com/dhis2/user-app/compare/v1.4.17...v1.4.18) (2022-06-29)


### Bug Fixes

* **translations:** sync translations from transifex (master) ([f461f7f](https://github.com/dhis2/user-app/commit/f461f7fb3376a68c3fcc4bc725c4b7e484e37c7b))

## [1.4.17](https://github.com/dhis2/user-app/compare/v1.4.16...v1.4.17) (2022-06-28)


### Bug Fixes

* upgrade @dhis2/ui to allow uppercase in username validation ([#1018](https://github.com/dhis2/user-app/issues/1018)) ([f80d732](https://github.com/dhis2/user-app/commit/f80d732eb46c08133354740dd23cb2a4691eba60))

## [1.4.16](https://github.com/dhis2/user-app/compare/v1.4.15...v1.4.16) (2022-06-25)


### Bug Fixes

* **translations:** sync translations from transifex (master) ([d5a6238](https://github.com/dhis2/user-app/commit/d5a6238ebef59be51824ca6cd5495afe6cc579f9))

## [1.4.15](https://github.com/dhis2/user-app/compare/v1.4.14...v1.4.15) (2022-06-23)


### Bug Fixes

* **translations:** sync translations from transifex (master) ([6fe8309](https://github.com/dhis2/user-app/commit/6fe83096835ae6136a9be1c7b3a39ed9014b13a4))

## [1.4.14](https://github.com/dhis2/user-app/compare/v1.4.13...v1.4.14) (2022-06-17)


### Bug Fixes

* **translations:** sync translations from transifex (master) ([08d511a](https://github.com/dhis2/user-app/commit/08d511a7036054039bee9a3b4aff016a2e142fae))

## [1.4.13](https://github.com/dhis2/user-app/compare/v1.4.12...v1.4.13) (2022-06-15)


### Bug Fixes

* **translations:** sync translations from transifex (master) ([80bfd15](https://github.com/dhis2/user-app/commit/80bfd1526e123edd466063ce8760bca111733db9))

## [1.4.12](https://github.com/dhis2/user-app/compare/v1.4.11...v1.4.12) (2022-06-14)


### Bug Fixes

* make username optional when inviting users ([#978](https://github.com/dhis2/user-app/issues/978)) ([dbdcccf](https://github.com/dhis2/user-app/commit/dbdcccfa294c1e4413cbf4fec391795f4dff3d34))

## [1.4.11](https://github.com/dhis2/user-app/compare/v1.4.10...v1.4.11) (2022-06-09)


### Bug Fixes

* **translations:** sync translations from transifex (master) ([3aed1ad](https://github.com/dhis2/user-app/commit/3aed1ad26c3f5b41caf4867da15f54d4b69bbc44))

## [1.4.10](https://github.com/dhis2/user-app/compare/v1.4.9...v1.4.10) (2022-05-31)


### Bug Fixes

* **translations:** sync translations from transifex (master) ([4dd3a25](https://github.com/dhis2/user-app/commit/4dd3a25b608facd433eb9a526903271a317930d0))

## [1.4.9](https://github.com/dhis2/user-app/compare/v1.4.8...v1.4.9) (2022-05-27)


### Bug Fixes

* **translations:** sync translations from transifex (master) ([05d621f](https://github.com/dhis2/user-app/commit/05d621f1b0a446d5560781fe6ba87e95f0cddd43))

## [1.4.8](https://github.com/dhis2/user-app/compare/v1.4.7...v1.4.8) (2022-05-24)


### Bug Fixes

* **translations:** sync translations from transifex (master) ([6ebfa7e](https://github.com/dhis2/user-app/commit/6ebfa7ee3a094ded3257e42ea481c176cc5b6f0e))

## [1.4.7](https://github.com/dhis2/user-app/compare/v1.4.6...v1.4.7) (2022-05-19)


### Bug Fixes

* **translations:** sync translations from transifex (master) ([0dd7c93](https://github.com/dhis2/user-app/commit/0dd7c933f9a5331df09dddfd50973063be0b8783))

## [1.4.6](https://github.com/dhis2/user-app/compare/v1.4.5...v1.4.6) (2022-05-17)


### Bug Fixes

* **translations:** sync translations from transifex (master) ([24d2728](https://github.com/dhis2/user-app/commit/24d2728f70d0e810f492199af65cbbdf3b4cd702))

## [1.4.5](https://github.com/dhis2/user-app/compare/v1.4.4...v1.4.5) (2022-05-16)


### Bug Fixes

* bulk-member-manager improvements ([#948](https://github.com/dhis2/user-app/issues/948)) ([49cae25](https://github.com/dhis2/user-app/commit/49cae250f6156ce8f7c1f735fd043b36d9c84c00))

## [1.4.4](https://github.com/dhis2/user-app/compare/v1.4.3...v1.4.4) (2022-05-02)


### Bug Fixes

* **translations:** sync translations from transifex (master) ([df9ec2d](https://github.com/dhis2/user-app/commit/df9ec2d61b774a530ab0cb2229df7ea493b09d87))

## [1.4.3](https://github.com/dhis2/user-app/compare/v1.4.2...v1.4.3) (2022-04-25)


### Bug Fixes

* **translations:** sync translations from transifex (master) ([1784b52](https://github.com/dhis2/user-app/commit/1784b525e2ba0334eec73d4ec47be1f174de00fc))

## [1.4.2](https://github.com/dhis2/user-app/compare/v1.4.1...v1.4.2) (2022-04-23)


### Bug Fixes

* **translations:** sync translations from transifex (master) ([b575268](https://github.com/dhis2/user-app/commit/b57526885cb38db7e7b72e9f4e95bb341e6d448b))

## [1.4.1](https://github.com/dhis2/user-app/compare/v1.4.0...v1.4.1) (2022-04-22)


### Bug Fixes

* **translations:** sync translations from transifex (master) ([3f9bbdc](https://github.com/dhis2/user-app/commit/3f9bbdca39214874b66e9dfe078f41b819ff0e22))

# [1.4.0](https://github.com/dhis2/user-app/compare/v1.3.18...v1.4.0) (2022-04-21)


### Features

* **user-group:** bulk group member manager ([#854](https://github.com/dhis2/user-app/issues/854)) ([0141a96](https://github.com/dhis2/user-app/commit/0141a961d09fbf51e38771654b948695b7d66827))

## [1.3.18](https://github.com/dhis2/user-app/compare/v1.3.17...v1.3.18) (2022-04-21)


### Bug Fixes

* **translations:** sync translations from transifex (master) ([917e542](https://github.com/dhis2/user-app/commit/917e542aa71b2e9f6b4e210848df1f7e22a2b447))

## [1.3.17](https://github.com/dhis2/user-app/compare/v1.3.16...v1.3.17) (2022-04-11)


### Bug Fixes

* **user-list:** show title at top of 'replicate user' form ([#925](https://github.com/dhis2/user-app/issues/925)) ([dd93d8c](https://github.com/dhis2/user-app/commit/dd93d8c7067fc88e387498d88f61c82d39344b74))

## [1.3.16](https://github.com/dhis2/user-app/compare/v1.3.15...v1.3.16) (2022-04-11)


### Bug Fixes

* **user-list:** only show 'reset password' action if an email can be sent ([#924](https://github.com/dhis2/user-app/issues/924)) ([27ccccb](https://github.com/dhis2/user-app/commit/27ccccbd182f1e60be40a77279c3d82d8e0f96f8))

## [1.3.15](https://github.com/dhis2/user-app/compare/v1.3.14...v1.3.15) (2022-03-31)


### Bug Fixes

* **attributes:** don't assume presence of values.attributeValues ([#927](https://github.com/dhis2/user-app/issues/927)) ([def822e](https://github.com/dhis2/user-app/commit/def822eeefe0b076590cf27b1b14e067e85ce6ff))

## [1.3.14](https://github.com/dhis2/user-app/compare/v1.3.13...v1.3.14) (2022-03-26)


### Bug Fixes

* **translations:** sync translations from transifex (master) ([8052a33](https://github.com/dhis2/user-app/commit/8052a3387ca1b7160a0cd48b8806750611a0dc72))

## [1.3.13](https://github.com/dhis2/user-app/compare/v1.3.12...v1.3.13) (2022-03-25)


### Bug Fixes

* **translations:** sync translations from transifex (master) ([a983cad](https://github.com/dhis2/user-app/commit/a983cadad611055939359eacfc264e5b6f175ea3))

## [1.3.12](https://github.com/dhis2/user-app/compare/v1.3.11...v1.3.12) (2022-03-24)


### Bug Fixes

* **translations:** sync translations from transifex (master) ([f8a9e51](https://github.com/dhis2/user-app/commit/f8a9e5131ddd8568b4f04065518dca8112b16950))

## [1.3.11](https://github.com/dhis2/user-app/compare/v1.3.10...v1.3.11) (2022-03-21)


### Bug Fixes

* **user:** show user's database language ([#917](https://github.com/dhis2/user-app/issues/917)) ([a73f506](https://github.com/dhis2/user-app/commit/a73f506e7342a08aa4ec473c4ab60798eef1f4d6))

## [1.3.10](https://github.com/dhis2/user-app/compare/v1.3.9...v1.3.10) (2022-03-14)


### Bug Fixes

* **translations:** sync translations from transifex (master) ([d3ff78e](https://github.com/dhis2/user-app/commit/d3ff78ef109cecc85fcd25c142d4b3db55a8846f))

## [1.3.9](https://github.com/dhis2/user-app/compare/v1.3.8...v1.3.9) (2022-03-10)


### Bug Fixes

* **form:** increase width of date input types due to Chrome calendar icon ([#914](https://github.com/dhis2/user-app/issues/914)) ([6866b85](https://github.com/dhis2/user-app/commit/6866b850303679dc5825177ac75116af82e8a187))
* **groups:** use correct method and endpoint to leave groups ([#912](https://github.com/dhis2/user-app/issues/912)) ([4fd020a](https://github.com/dhis2/user-app/commit/4fd020a110176d194f4efe2ddac3d221d2c57c1e))

## [1.3.8](https://github.com/dhis2/user-app/compare/v1.3.7...v1.3.8) (2022-03-06)


### Bug Fixes

* **translations:** sync translations from transifex (master) ([097372c](https://github.com/dhis2/user-app/commit/097372c5840c0ebd4da4d20aad4944f462bb68e0))

## [1.3.7](https://github.com/dhis2/user-app/compare/v1.3.6...v1.3.7) (2022-02-28)


### Bug Fixes

* **translations:** sync translations from transifex (master) ([11e61e1](https://github.com/dhis2/user-app/commit/11e61e195ff44070b02300239896d6271f29b482))

## [1.3.6](https://github.com/dhis2/user-app/compare/v1.3.5...v1.3.6) (2022-02-25)


### Bug Fixes

* **user:** org unit related fixes ([#900](https://github.com/dhis2/user-app/issues/900)) ([d35ddf5](https://github.com/dhis2/user-app/commit/d35ddf53e1a0cd1cda250c39e2b01e78d89e8b73))

## [1.3.5](https://github.com/dhis2/user-app/compare/v1.3.4...v1.3.5) (2022-02-25)


### Bug Fixes

* **user:** only set email field as required if inviting by email ([62e79a1](https://github.com/dhis2/user-app/commit/62e79a17c011e1bd1fe5ad558c2b7296daf76f02))

## [1.3.4](https://github.com/dhis2/user-app/compare/v1.3.3...v1.3.4) (2022-02-23)


### Bug Fixes

* **translations:** sync translations from transifex (master) ([fb8b1e3](https://github.com/dhis2/user-app/commit/fb8b1e35540480f11e467696d13544f85880a20c))

## [1.3.3](https://github.com/dhis2/user-app/compare/v1.3.2...v1.3.3) (2022-02-22)


### Bug Fixes

* **translations:** sync translations from transifex (master) ([a4ead95](https://github.com/dhis2/user-app/commit/a4ead95a93baaf3682577b299d4909923d0cc69e))

## [1.3.2](https://github.com/dhis2/user-app/compare/v1.3.1...v1.3.2) (2022-02-19)


### Bug Fixes

* **translations:** sync translations from transifex (master) ([74b9a21](https://github.com/dhis2/user-app/commit/74b9a217a3981645a9d660eadbf92c54bb6a985d))

## [1.3.1](https://github.com/dhis2/user-app/compare/v1.3.0...v1.3.1) (2022-02-18)


### Bug Fixes

* **translations:** sync translations from transifex (master) ([6d6d22e](https://github.com/dhis2/user-app/commit/6d6d22ef43f7732dfcfea235342b20a09968c603))

# [1.3.0](https://github.com/dhis2/user-app/compare/v1.2.14...v1.3.0) (2022-02-17)


### Bug Fixes

* remove beta entries from changelog ([6b67e82](https://github.com/dhis2/user-app/commit/6b67e8295c4f1402e8c46a2c22c121e73c4493d0))
* **semantic:** allow merge commits ([6f50bf3](https://github.com/dhis2/user-app/commit/6f50bf349e7b76697594cf6838a6f995e3e35e33))
* don't publish to App Hub ([bcbdd61](https://github.com/dhis2/user-app/commit/bcbdd6105b8ba3a5360fa0a4294032c024785358))
* **home:** migrate homepage components to @dhis2/ui ([#840](https://github.com/dhis2/user-app/issues/840)) ([8f2f7d9](https://github.com/dhis2/user-app/commit/8f2f7d9464d18076ec79e52de0313c25a07598a3))
* **sidenav:** add missing "return null" statement when there are no sections ([a826c5e](https://github.com/dhis2/user-app/commit/a826c5e4777a95f84a3a29b0bc6a981ea07216a4))


### Features

* add css variables to be used in css modules ([#834](https://github.com/dhis2/user-app/issues/834)) ([8fdc378](https://github.com/dhis2/user-app/commit/8fdc378d6e279dcd23bb89ad5db5eef65f1c92d5))
* migrate searchable-org-unit-tree to @dhis2/ui ([#836](https://github.com/dhis2/user-app/issues/836)) ([fe2d22c](https://github.com/dhis2/user-app/commit/fe2d22c3aeca2686117d05cb9afbc8bfac536a94))
* migrate tables and search filter controls to @dhis2/ui ([#830](https://github.com/dhis2/user-app/issues/830)) ([f3a9f0b](https://github.com/dhis2/user-app/commit/f3a9f0b4eaea31662a640cae8fb2d18ae602897a))
* migrate user-group form to dhis2/ui ([#831](https://github.com/dhis2/user-app/issues/831)) ([56f4b83](https://github.com/dhis2/user-app/commit/56f4b830955a3b76ffa70356505a5c9fe59b899a))

## [1.2.14](https://github.com/dhis2/user-app/compare/v1.2.13...v1.2.14) (2022-02-17)


### Bug Fixes

* **semantic:** allow merge commits ([732ae7f](https://github.com/dhis2/user-app/commit/732ae7f6cc03d4f27f48dab92abf6264f530e027))

## [1.2.13](https://github.com/dhis2/user-app/compare/v1.2.12...v1.2.13) (2022-02-15)


### Bug Fixes

* **translations:** sync translations from transifex (master) ([d6fe01c](https://github.com/dhis2/user-app/commit/d6fe01c9e86faf28d47b8f39586e0f2ad1fb9597))

## [1.2.12](https://github.com/dhis2/user-app/compare/v1.2.11...v1.2.12) (2022-01-13)


### Bug Fixes

* **translations:** sync translations from transifex (master) ([964ff3c](https://github.com/dhis2/user-app/commit/964ff3cbc0da03f2dea1040a30fcec6413f6b1fb))

## [1.2.11](https://github.com/dhis2/user-app/compare/v1.2.10...v1.2.11) (2021-12-07)


### Bug Fixes

* **translations:** sync translations from transifex (master) ([32c22c2](https://github.com/dhis2/user-app/commit/32c22c2492aedacdd69012f173230696ba378938))

## [1.2.10](https://github.com/dhis2/user-app/compare/v1.2.9...v1.2.10) (2021-10-27)


### Bug Fixes

* **translations:** sync translations from transifex (master) ([7432a20](https://github.com/dhis2/user-app/commit/7432a205e732e60742ccc6a43b6d1a81e930530f))

## [1.2.9](https://github.com/dhis2/user-app/compare/v1.2.8...v1.2.9) (2021-10-08)


### Bug Fixes

* **translations:** sync translations from transifex (master) ([fb1f63a](https://github.com/dhis2/user-app/commit/fb1f63a41620af57555efbbce9702c9778ff510a))

## [1.2.8](https://github.com/dhis2/user-app/compare/v1.2.7...v1.2.8) (2021-10-02)


### Bug Fixes

* **translations:** sync translations from transifex (master) ([7577d76](https://github.com/dhis2/user-app/commit/7577d760b08c0d9d645d30a455436feb19a63139))

## [1.2.7](https://github.com/dhis2/user-app/compare/v1.2.6...v1.2.7) (2021-10-01)


### Bug Fixes

* **translations:** sync translations from transifex (master) ([c145f10](https://github.com/dhis2/user-app/commit/c145f104c3e2cfd06a8b2f9d1791847d8c214000))

## [1.2.6](https://github.com/dhis2/user-app/compare/v1.2.5...v1.2.6) (2021-09-30)


### Bug Fixes

* **translations:** sync translations from transifex (master) ([b5680bf](https://github.com/dhis2/user-app/commit/b5680bf10b9fd337f008445a7cde1cc7d2b3d980))

## [1.2.5](https://github.com/dhis2/user-app/compare/v1.2.4...v1.2.5) (2021-09-29)


### Bug Fixes

* rename 'surname' field to 'last name' and show after 'first name' field ([6bc2046](https://github.com/dhis2/user-app/commit/6bc2046443d6d7f8d180253ebdb608e4f34ccb8b))

## [1.2.4](https://github.com/dhis2/user-app/compare/v1.2.3...v1.2.4) (2021-09-12)


### Bug Fixes

* **translations:** sync translations from transifex (master) ([6c1c5b5](https://github.com/dhis2/user-app/commit/6c1c5b5e463e921a416d0874706292347fed75db))

## [1.2.3](https://github.com/dhis2/user-app/compare/v1.2.2...v1.2.3) (2021-09-05)


### Bug Fixes

* **translations:** sync translations from transifex (master) ([d68a48a](https://github.com/dhis2/user-app/commit/d68a48a356457ab9b0437a534fe85df4d0356dbd))

## [1.2.2](https://github.com/dhis2/user-app/compare/v1.2.1...v1.2.2) (2021-09-04)


### Bug Fixes

* **translations:** sync translations from transifex (master) ([cd73b95](https://github.com/dhis2/user-app/commit/cd73b95249b3cba4235f544d64b88405f92fd555))

## [1.2.1](https://github.com/dhis2/user-app/compare/v1.2.0...v1.2.1) (2021-09-01)


### Bug Fixes

* **translations:** sync translations from transifex (master) ([63dab6f](https://github.com/dhis2/user-app/commit/63dab6f2eff73037fcc1ee885189b671aa68c71e))

# [1.2.0](https://github.com/dhis2/user-app/compare/v1.1.21...v1.2.0) (2021-08-31)


### Features

* **reset-password:** allow resetting password via context menu ([#805](https://github.com/dhis2/user-app/issues/805)) ([d5fb507](https://github.com/dhis2/user-app/commit/d5fb5072d844eacf771d59ce52f36632b0f1ffca))

## [1.1.21](https://github.com/dhis2/user-app/compare/v1.1.20...v1.1.21) (2021-08-31)


### Bug Fixes

* **translations:** sync translations from transifex (master) ([995df0e](https://github.com/dhis2/user-app/commit/995df0e1218332ebcde9c6a0822082fa0ccf03f5))

## [1.1.20](https://github.com/dhis2/user-app/compare/v1.1.19...v1.1.20) (2021-08-12)


### Bug Fixes

* **translations:** sync translations from transifex (master) ([fb14a8e](https://github.com/dhis2/user-app/commit/fb14a8e5db59f7416af88c82b8cfbde4c1981a88))

## [1.1.19](https://github.com/dhis2/user-app/compare/v1.1.18...v1.1.19) (2021-07-26)


### Bug Fixes

* leave URI encoding to d2 API ([#790](https://github.com/dhis2/user-app/issues/790)) ([756a243](https://github.com/dhis2/user-app/commit/756a2432b78dd5910dfc21b108344ccaab23feb8))

## [1.1.18](https://github.com/dhis2/user-app/compare/v1.1.17...v1.1.18) (2021-07-21)


### Bug Fixes

* **translations:** sync translations from transifex (master) ([b9d70b2](https://github.com/dhis2/user-app/commit/b9d70b252717264863177dad66c1ff7648363dc6))

## [1.1.17](https://github.com/dhis2/user-app/compare/v1.1.16...v1.1.17) (2021-07-06)


### Bug Fixes

* bump cli-app-scripts to 7.1.0 ([#784](https://github.com/dhis2/user-app/issues/784)) ([e7085ea](https://github.com/dhis2/user-app/commit/e7085eacde3c9d9c8d1c3306cb5f216f0f6c39c0))

## [1.1.16](https://github.com/dhis2/user-app/compare/v1.1.15...v1.1.16) (2021-06-30)


### Bug Fixes

* **translations:** sync translations from transifex (master) ([4ec2ff2](https://github.com/dhis2/user-app/commit/4ec2ff27149dcb7d15231707164fcfc893af72d3))

## [1.1.15](https://github.com/dhis2/user-app/compare/v1.1.14...v1.1.15) (2021-06-29)


### Bug Fixes

* **translations:** sync translations from transifex (master) ([336641e](https://github.com/dhis2/user-app/commit/336641e79cfdfb9716ea74455112a8f884e3cac9))

## [1.1.14](https://github.com/dhis2/user-app/compare/v1.1.13...v1.1.14) (2021-06-25)


### Bug Fixes

* **translations:** sync translations from transifex (master) ([d19050a](https://github.com/dhis2/user-app/commit/d19050ab0bfd60a5737064e59db3f3322dbba903))

## [1.1.13](https://github.com/dhis2/user-app/compare/v1.1.12...v1.1.13) (2021-06-08)


### Bug Fixes

* **translations:** sync translations from transifex (master) ([4b27c92](https://github.com/dhis2/user-app/commit/4b27c927ecc5384e4b9ede67e64ffb2ebbf57bd0))

## [1.1.12](https://github.com/dhis2/user-app/compare/v1.1.11...v1.1.12) (2021-05-18)


### Bug Fixes

* **translations:** sync translations from transifex (master) ([599cb3a](https://github.com/dhis2/user-app/commit/599cb3a6e54294b030ab4360ed13bca0425b94ef))

## [1.1.11](https://github.com/dhis2/user-app/compare/v1.1.10...v1.1.11) (2021-04-20)


### Bug Fixes

* **i18n:** allow unresolved import on generated file ([c85e264](https://github.com/dhis2/user-app/commit/c85e2647ec5285c6863e2782351fed0290fdd3b7))
* **i18n:** ensure resources are added by importing from index ([fce4b93](https://github.com/dhis2/user-app/commit/fce4b93a1948a627b90ba67f0ef5c36c44292d7d))

## [1.1.10](https://github.com/dhis2/user-app/compare/v1.1.9...v1.1.10) (2021-03-27)


### Bug Fixes

* **translations:** sync translations from transifex (master) ([84dd701](https://github.com/dhis2/user-app/commit/84dd701a235118d7575ee69db90971aca12efbf4))

## [1.1.9](https://github.com/dhis2/user-app/compare/v1.1.8...v1.1.9) (2021-03-19)


### Bug Fixes

* **expiry date:** do not parse empty initial state to date ([c68a4ca](https://github.com/dhis2/user-app/commit/c68a4ca20a329f53798aa5b684151bd283088e49))

## [1.1.8](https://github.com/dhis2/user-app/compare/v1.1.7...v1.1.8) (2021-03-13)


### Bug Fixes

* **translations:** sync translations from transifex (master) ([e3e87cd](https://github.com/dhis2/user-app/commit/e3e87cd7add40fd9b528c4ac67d6b1ad9ce12687))

## [1.1.7](https://github.com/dhis2/user-app/compare/v1.1.6...v1.1.7) (2021-03-12)


### Bug Fixes

* **translations:** sync translations from transifex (master) ([0f35858](https://github.com/dhis2/user-app/commit/0f3585800865c4568b971341b28a081b203821f1))

## [1.1.6](https://github.com/dhis2/user-app/compare/v1.1.5...v1.1.6) (2021-03-11)


### Bug Fixes

* **deps:** upgrade deps to enable treeshaking and headerbar fixes ([71a8c2f](https://github.com/dhis2/user-app/commit/71a8c2f99615274f02fc160e4a7a8f123dc9f59f))

## [1.1.5](https://github.com/dhis2/user-app/compare/v1.1.4...v1.1.5) (2021-03-01)


### Bug Fixes

* **translations:** sync translations from transifex (master) ([806760e](https://github.com/dhis2/user-app/commit/806760e5cf02f42efa6ab39258ff995adcbf41a6))

## [1.1.4](https://github.com/dhis2/user-app/compare/v1.1.3...v1.1.4) (2021-02-28)


### Bug Fixes

* **translations:** sync translations from transifex (master) ([38ccc35](https://github.com/dhis2/user-app/commit/38ccc355b00596639b4aab7182aca37e4945c6b2))

## [1.1.3](https://github.com/dhis2/user-app/compare/v1.1.2...v1.1.3) (2021-02-26)


### Bug Fixes

* **translations:** sync translations from transifex (master) ([b83b19d](https://github.com/dhis2/user-app/commit/b83b19db29099ea617619ef6a2b111e766e1b03e))

## [1.1.2](https://github.com/dhis2/user-app/compare/v1.1.1...v1.1.2) (2021-02-25)


### Bug Fixes

* **user:** improve regex for email validation ([a378e38](https://github.com/dhis2/user-app/commit/a378e38b0b5fe95decf66b5da0919c2702927259))

## [1.1.1](https://github.com/dhis2/user-app/compare/v1.1.0...v1.1.1) (2021-02-24)


### Bug Fixes

* **translations:** sync translations from transifex (master) ([7493713](https://github.com/dhis2/user-app/commit/7493713c0881b0c4e32ea2c1d9ed55905608f751))

# [1.1.0](https://github.com/dhis2/user-app/compare/v1.0.16...v1.1.0) (2021-02-23)


### Features

* **user form:** add account expiry date field (edit & new) ([5e710c0](https://github.com/dhis2/user-app/commit/5e710c0a9c6fc4b308cff1260f18e9ff73f560e3))

## [1.0.16](https://github.com/dhis2/user-app/compare/v1.0.15...v1.0.16) (2021-02-23)


### Bug Fixes

* **user:** change OpenID field label to OIDC mapping value ([5b72b15](https://github.com/dhis2/user-app/commit/5b72b15fdc1b2658c9e73970d6ed672a9b56673e))

## [1.0.15](https://github.com/dhis2/user-app/compare/v1.0.14...v1.0.15) (2021-02-21)


### Bug Fixes

* **translations:** sync translations from transifex (master) ([aba40e8](https://github.com/dhis2/user-app/commit/aba40e8cf37322b8491068b9fc86e522b20a0db8))

## [1.0.14](https://github.com/dhis2/user-app/compare/v1.0.13...v1.0.14) (2021-02-20)


### Bug Fixes

* **translations:** sync translations from transifex (master) ([34137c6](https://github.com/dhis2/user-app/commit/34137c634833747f8bd85d9bc56dadcf8ded6661))

## [1.0.13](https://github.com/dhis2/user-app/compare/v1.0.12...v1.0.13) (2021-02-19)


### Bug Fixes

* exclude users from group queries, not role queries ([9ae39ca](https://github.com/dhis2/user-app/commit/9ae39ca81dc3519e309fb1d8d9709531383dfaf6))
* **user-group:** remove users field from form to prevent app freezing ([48af65c](https://github.com/dhis2/user-app/commit/48af65c41cc25eadb724d9de8e2406728cd9aac5))
* **user-groups:** exclude users field when getting user-group details ([1d6eaef](https://github.com/dhis2/user-app/commit/1d6eaeff7a5d938ce5fb4fd0a29c9e35ba6265c2))

## [1.0.12](https://github.com/dhis2/user-app/compare/v1.0.11...v1.0.12) (2021-02-15)


### Bug Fixes

* **translations:** sync translations from transifex (master) ([454cb00](https://github.com/dhis2/user-app/commit/454cb00d08183ac7aec2babf56bbb2cf5069fdfd))

## [1.0.11](https://github.com/dhis2/user-app/compare/v1.0.10...v1.0.11) (2021-02-13)


### Bug Fixes

* **translations:** sync translations from transifex (master) ([f02e26f](https://github.com/dhis2/user-app/commit/f02e26f1cd225b3483462bf3546b966e29bd76e5))

## [1.0.10](https://github.com/dhis2/user-app/compare/v1.0.9...v1.0.10) (2021-02-12)


### Bug Fixes

* **translations:** sync translations from transifex (master) ([7cd3f01](https://github.com/dhis2/user-app/commit/7cd3f01e864a93da23e8ff8378efe62c86a34c2e))

## [1.0.9](https://github.com/dhis2/user-app/compare/v1.0.8...v1.0.9) (2021-02-11)


### Bug Fixes

* don't fetch unpaged members of current user groups (DHIS2-10377) ([#672](https://github.com/dhis2/user-app/issues/672)) ([bdff1ba](https://github.com/dhis2/user-app/commit/bdff1ba29c05e8e447c42645abdee540f3d8d140))

## [1.0.8](https://github.com/dhis2/user-app/compare/v1.0.7...v1.0.8) (2021-02-01)


### Bug Fixes

* **translations:** sync translations from transifex (master) ([f93c222](https://github.com/dhis2/user-app/commit/f93c222811a07109db1247617e09b1d1e7df5bbc))

## [1.0.7](https://github.com/dhis2/user-app/compare/v1.0.6...v1.0.7) (2021-01-26)


### Bug Fixes

* **translations:** sync translations from transifex (master) ([09f5fe3](https://github.com/dhis2/user-app/commit/09f5fe354f434a47435202e860bbe6deb978ec52))

## [1.0.6](https://github.com/dhis2/user-app/compare/v1.0.5...v1.0.6) (2021-01-18)


### Bug Fixes

* **user:** add disabled field to user form ([db15f0e](https://github.com/dhis2/user-app/commit/db15f0ef8d4e5bc8f5fdefbf3b7c5e4862d6675c))
* add checkbox in UI for disabled status (DHIS2-7940) ([f9b51ae](https://github.com/dhis2/user-app/commit/f9b51ae4190d1a7643e27a08357d079eb9c7ecc8))
* maintain disabled status upon save of user (DHIS2-7940) ([9c4f0c7](https://github.com/dhis2/user-app/commit/9c4f0c78a4d7e0a597e104871173f0a61fce1e3c))

## [1.0.5](https://github.com/dhis2/user-app/compare/v1.0.4...v1.0.5) (2021-01-18)


### Bug Fixes

* **translations:** sync translations from transifex (master) ([0816d96](https://github.com/dhis2/user-app/commit/0816d96fed0b534f18a4b4c0400ee6f0209dafe4))

## [1.0.4](https://github.com/dhis2/user-app/compare/v1.0.3...v1.0.4) (2021-01-08)


### Bug Fixes

* **translations:** sync translations from transifex (master) ([8b78ada](https://github.com/dhis2/user-app/commit/8b78adad7be4337bd144a63a2b3bc50aab4725b5))

## [1.0.3](https://github.com/dhis2/user-app/compare/v1.0.2...v1.0.3) (2020-12-24)


### Bug Fixes

* **translations:** sync translations from transifex (master) ([4817fa6](https://github.com/dhis2/user-app/commit/4817fa6cf4c8572234b7d5cf5e3e7636c475e836))

## [1.0.2](https://github.com/dhis2/user-app/compare/v1.0.1...v1.0.2) (2020-12-21)


### Bug Fixes

* **translations:** sync translations from transifex (master) ([1a09941](https://github.com/dhis2/user-app/commit/1a0994178c0eebb49ee3d72bb9225358a318a99a))

## [1.0.1](https://github.com/dhis2/user-app/compare/v1.0.0...v1.0.1) (2020-12-05)


### Bug Fixes

* **translations:** sync translations from transifex (master) ([53f0448](https://github.com/dhis2/user-app/commit/53f0448dca1fc5153e11853b2c37acffa376dca3))

# 1.0.0 (2020-12-01)


### Bug Fixes

* **group-editor:** convert d2 Model to object before setting displayName ([b714b6e](https://github.com/dhis2/user-app/commit/b714b6e066e2c7b9251de6bdecf44e86bb1e165a))
* **translations:** sync translations from transifex (master) ([064eaa4](https://github.com/dhis2/user-app/commit/064eaa4980cc762b60794323523b412129c926b4))
* **user:** remove generateUid import and remove ids from POST payload ([44978e7](https://github.com/dhis2/user-app/commit/44978e72dbdcc2c33cf3eb4bec2a9948556f7a5c))
* add app description to d2 config ([21c8bd6](https://github.com/dhis2/user-app/commit/21c8bd6a1abd85ec315876b317914f78099c5e91))
* adjust d2 config ([1446cd3](https://github.com/dhis2/user-app/commit/1446cd341aa26a0036a27b3bf276e449cad13f96))
* format files using cli-style ([2bcc00f](https://github.com/dhis2/user-app/commit/2bcc00f03408bff8583d497a18ecb8466000f8da))
* get d2 i18n translations to work ([e66c546](https://github.com/dhis2/user-app/commit/e66c546014da6bfe75d0acd68f6942942cd7f778))
* remove duplicate entry from gitignore ([2969eee](https://github.com/dhis2/user-app/commit/2969eeeae411f6c3e04261fd440a34e9497671f8))
* remove incorrect description comment from app component ([0dc85aa](https://github.com/dhis2/user-app/commit/0dc85aa96728b1ae08437f19eefc3c272a539b18))
* show loader when d2 is not available yet ([3c39a86](https://github.com/dhis2/user-app/commit/3c39a86579774795a03c98db4327d835f274b442))
* **detail-summary:** fix i18n on edit button label ([2a7d599](https://github.com/dhis2/user-app/commit/2a7d599a2233e8785e37bc9e6129448d73f8d197))
* **detail-views:** add `access` property to model so detail-views work ([47761cf](https://github.com/dhis2/user-app/commit/47761cf12d102d6eeb5d9284a41c97a7def3bfbf))
* **headerbar:** upgrade version so it gets latest features and fixes ([d74cd64](https://github.com/dhis2/user-app/commit/d74cd6495987ec6000c65467eebde07d7200ff81))
* **i18n:** add transifex config for .properties files ([df769af](https://github.com/dhis2/user-app/commit/df769af0ba143268835038c3a056c8705368b229))
* **i18n:** add transifex config for .properties files ([70ef42b](https://github.com/dhis2/user-app/commit/70ef42b102165137ae60182f3304fb80ed77a750))
* **i18n:** correct edit button text on DetailSummary ([85663d0](https://github.com/dhis2/user-app/commit/85663d0099475628db184b332448405e1fc179b7))
* **i18n:** make sure i18n.t is called after the locale has been set ([f1709d9](https://github.com/dhis2/user-app/commit/f1709d99618932cb9a48a24eaea9c977062b67ad))
* **i18n:** make sure i18n.t is called after the locale has been set ([922ec63](https://github.com/dhis2/user-app/commit/922ec636714761b7bcf843e1de07d55d2bd0af1c))
* **translations:** produce and stage pot files in pre-commit hook ([283d611](https://github.com/dhis2/user-app/commit/283d611a200eca5f1636d18b1869599e19f7b038))
* **translations:** produce and stage pot files in pre-commit hook ([bfa33eb](https://github.com/dhis2/user-app/commit/bfa33eb784dfc888c0377d62434314f7c3a9f275))
* **translations:** stage translations using semi-colon syntax ([374a844](https://github.com/dhis2/user-app/commit/374a8446bd46ef436e72f36d2b2ea2da9fc42011))
* **translations:** sync translations from transifex (master) ([63a7d8a](https://github.com/dhis2/user-app/commit/63a7d8ad25cd519e89148a93cf10055f239cd520))
* **translations:** sync translations from transifex (master) ([9bc6dd9](https://github.com/dhis2/user-app/commit/9bc6dd94054a784087840b281799559a6a0d7061))
* **translations:** sync translations from transifex (master) ([c7d38e5](https://github.com/dhis2/user-app/commit/c7d38e5db0f58a3572ad834e8a6c946f359c3b25))
* **translations:** sync translations from transifex (master) ([9095745](https://github.com/dhis2/user-app/commit/909574540f6effc7113ef743a20c9ae6327a48b9))
* **translations:** sync translations from transifex (master) ([0ec5d66](https://github.com/dhis2/user-app/commit/0ec5d667aaad47ff667fe4fb7a86b7ac89fc4bc6))
* **translations:** sync translations from transifex (master) ([acf1e38](https://github.com/dhis2/user-app/commit/acf1e38187165c68b621866fd4e53e8cce612e48))
* **translations:** sync translations from transifex (master) ([f02847c](https://github.com/dhis2/user-app/commit/f02847ce414f153a3f03debe2c6ddbc62719f7ec))
* **translations:** sync translations from transifex (master) ([2dbf7ed](https://github.com/dhis2/user-app/commit/2dbf7ed2a486415620e9bf25521285ec16091419))
* **translations:** sync translations from transifex (master) ([46150c1](https://github.com/dhis2/user-app/commit/46150c16e9189bda1e7a240c153f7c29b8713180))
* **translations:** sync translations from transifex (master) ([6470540](https://github.com/dhis2/user-app/commit/64705403a48bf31f85a6551a3b1d633a70796471))
* **user:** make ui and db locale field required and prevent clear onBlur ([fc77658](https://github.com/dhis2/user-app/commit/fc77658b04ffa9ef5a6736612bd64cc367e5843b))
* **user-form:** make data capture and maintenance org unit mandatory ([325470f](https://github.com/dhis2/user-app/commit/325470f436eb659dec9c1b479c41da8cd9ea8acd))
* **user-form:** make data capture and maintenance org unit mandatory ([4a7f513](https://github.com/dhis2/user-app/commit/4a7f513547612239e800f3b7fdb48eb5fef19de0))
* identified static strings and made them translatable ([#431](https://github.com/dhis2/user-app/issues/431)) ([dcf6d79](https://github.com/dhis2/user-app/commit/dcf6d79247d6634ae673f4346eea28bae4ce75fd))
* **authorities:** add visualization and remove report-table and chart ([1f4c2bf](https://github.com/dhis2/user-app/commit/1f4c2bff9ac5b525fc89644baba333caefd805ea))
* **authorities:** add visualization and remove report-table and chart ([ac06b3b](https://github.com/dhis2/user-app/commit/ac06b3bba3014d9ed6a20f80339f4f8534ccd993))
* **forms:** prevent autocomplete ([eb561a6](https://github.com/dhis2/user-app/commit/eb561a6f77812df53f4c6ecd188be0248ece61f5))
* **forms:** prevent autocomplete ([8fa66ac](https://github.com/dhis2/user-app/commit/8fa66ace9259019929802d06c328297563206065))
* **replicate-user:** add autocomplete="new-password" to prevent autofill ([7e71aab](https://github.com/dhis2/user-app/commit/7e71aab3aa9b0e38138bfa784470e6dac95a9d74))
* **replicate-user:** add autocomplete="new-password" to prevent autofill ([4c6dda6](https://github.com/dhis2/user-app/commit/4c6dda63dbe2caf46a33caa791d99408d2813c07))
* **translation:** prevent JS error in list view for Indonesian language ([7ba4859](https://github.com/dhis2/user-app/commit/7ba4859729a178c0992479589b5d1fcb725fa6a3))
* **user:** allow clearing string values ([71da77c](https://github.com/dhis2/user-app/commit/71da77c45571c325d4b904dca9574a218e63455f))
* **user:** make ui and db locale field required and prevent clear onBlur ([d418a93](https://github.com/dhis2/user-app/commit/d418a9309951a1412a76375b62ac7251770eb620))
* add margin below list to avoid [+] button overlapping pagination ([797be9d](https://github.com/dhis2/user-app/commit/797be9d1afe428b66c4177c018fbf43c8110db8a))
* add margin below list to avoid [+] button overlapping pagination ([f8895af](https://github.com/dhis2/user-app/commit/f8895afe93c37007a249a981a5245143eabf5a23))
* adds margin-top to cards ([89a5ee9](https://github.com/dhis2/user-app/commit/89a5ee9214b0b77bd8e97f016d27241c01ea323c))
* fix typo on user section and make translatable ([3d7de0c](https://github.com/dhis2/user-app/commit/3d7de0ce46e249f5ad8ac0ebd04a0f9b6ee2ab09))
* ie11 support ([1a05669](https://github.com/dhis2/user-app/commit/1a05669068cfa8ab512fa837f2f40c8351b126c2))
* include all owned fields when getting group and role ([96829b9](https://github.com/dhis2/user-app/commit/96829b9db344ab39a4c449d414ac28967b20cec0))
* include all owned fields when getting group and role ([ee80bf7](https://github.com/dhis2/user-app/commit/ee80bf7838b679b92e337a650b7aa92edcf6540d))
* multiple async blur field with errors ([e7be09b](https://github.com/dhis2/user-app/commit/e7be09bd35f67b7ccbc0a0525ccb31c609378ae1))
* remove dead code after deprecated auths were removed from response ([62e574c](https://github.com/dhis2/user-app/commit/62e574ccb774a4d3fef1a4c45bd12c9d16421cd7))
* translations ([5725faa](https://github.com/dhis2/user-app/commit/5725faaf7c83e35a62f6c8a5370cf8b6e44a52aa)), closes [#58](https://github.com/dhis2/user-app/issues/58)
* translations ([15030b2](https://github.com/dhis2/user-app/commit/15030b2a9c745d4205a5c5aee178cd830d759d00))
* update headerbar to the latest and greatest ([#323](https://github.com/dhis2/user-app/issues/323)) ([195dbf1](https://github.com/dhis2/user-app/commit/195dbf13faf52826dc198ab63a3f090d65bd95d1))
* **attribute validation:** only validate patterns when fields have a value ([00ce0ad](https://github.com/dhis2/user-app/commit/00ce0ad1169469b8491e5b6b3727e17b3c7e66f5))
* **attributes:** remove list from concat params ([c2d16da](https://github.com/dhis2/user-app/commit/c2d16da938edd19c7f5ea21a26a186d24279df37))
* **d2:** first init d2 with correct baseUrl, then call getUserSettings ([636bb28](https://github.com/dhis2/user-app/commit/636bb2883cc8cd3a8956e3c382667568a3930a6f))
* **form-validation:** async blur fields clearing ([05d50cd](https://github.com/dhis2/user-app/commit/05d50cd2b4bb7372e5160cbf340fb959a69f4074))
* **layout:** fix whitespace above <main/> in menu-cards view ([fcc5027](https://github.com/dhis2/user-app/commit/fcc502713b79127a899d759001510105a6c4b34f))
* **lists:** also sort by firstName surname for super user ([b6f0dbb](https://github.com/dhis2/user-app/commit/b6f0dbb036b9afa018f6f256b05442f26eed4d71))
* **lists:** sort users by firstName>surname and others by name ([bdb0dd1](https://github.com/dhis2/user-app/commit/bdb0dd18dc9117006bb3939bd23a8f9f38978e74))
* **org-units:** stop assuming there can only be a single root org ([a03f0ae](https://github.com/dhis2/user-app/commit/a03f0ae6ef16af19a6ebc31bc2f45bf0a87c80e6))
* **password:** special character validation now aligned with server ([03700bf](https://github.com/dhis2/user-app/commit/03700bfdaf78634bd4384821ef192dc54f1ec170))
* **sharing dialog:** update component to latest d2-ui version ([e653468](https://github.com/dhis2/user-app/commit/e6534685abe05ed34a9f20cfec9986bd4c14d0f4))
* **sharing dialog:** update component to latest d2-ui version ([#54](https://github.com/dhis2/user-app/issues/54)) ([98c9c49](https://github.com/dhis2/user-app/commit/98c9c49a9cd2fa0b83e45dfdf6e863b1035dd07b))
* **style:** remove newline ([5aeb06d](https://github.com/dhis2/user-app/commit/5aeb06d022ec540c48148bd96dfc4012260ef1ee))
* **unique-attributes:** handle same value on different attribute correctly ([2b6ec30](https://github.com/dhis2/user-app/commit/2b6ec3089be7f30d3028d5b07a1f842c021580a0))
* **user-section:** allow uppercase in email validation ([d810290](https://github.com/dhis2/user-app/commit/d8102904a7a065d877b3b13396ff3263f9ae237d))


### Features

* **authorities:** adds skip-data-import-audit authority ([41de30a](https://github.com/dhis2/user-app/commit/41de30a608b06a0e3c098056fcd53869c314109e))
* **authority-editor:** remove performance hacks, add section checkboxes ([1200a11](https://github.com/dhis2/user-app/commit/1200a11327b2e2e6c2611688e2435c1ef51482f4))
* **authority-editor:** remove performance hacks, add section checkboxes ([1d6826e](https://github.com/dhis2/user-app/commit/1d6826e82a35ecdaf4b86ab2ad2675a2d56c0de5))
